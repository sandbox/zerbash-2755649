<?php

namespace Drupal\blackboard_connect_api;

use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Drupal\Core\Logger\LoggerChannelFactory;

class BlackboardConnectAPI {

  protected $client;
  protected $method;
  protected $params;
  protected $xml;
  protected $config;
  
  /**
   * Create and prepare this object's SOAP client.
   * 
   * @param string $wsdl
   */
  function __construct($wsdl, ConfigFactory $config, LoggerChannelFactory $loggerFactory) {

    $this->client = new \SoapClient($wsdl,[
      'stream_context' => stream_context_create(
				[
					'ssl' => [
						'verify_peer'       => false,
						'verify_peer_name'  => false,
					]
				]
      ),
      'trace' => 1
      
    ]);
    $this->config = $config->get('blackboard_connect_api.settings');
    $this->setHeaders();
    $this->encoder = new XmlEncoder;
    $this->logger = $loggerFactory->get('blackboard_connect_api');
  }
  
  public function update($contact){
    $this->logger->notice('updating ' . $contact['id']);
    $xml = $this->buildRecord($contact);
    $this->execute($xml);
  }
  
  public function delete($contact){
    $this->logger->notice('deleting ' . $contact['id']);
    $xml = $this->buildRecord($contact,'Delete');
    $this->execute($xml);
  }
  
  protected function buildRecord($contact,$action = 'Update'){

    $record = [
      '@Version' => '2.0',
      'Site' => [
        '@LocalID' => $this->config->get('local_id'),
        'Contact' => [
          '@Action' => $action,
          '@ReferenceCode' => $contact['id'],
          '@AcceptedTermsOfUse' => 'Yes',
          '@Type' => $contact['type'],
          '@DataProvider' => 'Drupal',
        ]
      ]
    ];
    if($action == 'Update'){
      $contactTypes = [
        'phones' => [],
        'sms' => [],
        'emails' => []
      ];     
      $contactTypes = [
        'phones' => 'Phone',
        'sms' => 'SMS',
        'emails' => 'Email'
      ];
      foreach($contactTypes as $key => $type){
        $types = [];
        foreach($contact[$key] as $i => $value){
          if($value){
            $types[] = [
              '@Index' => $i + 1, // Index must start at 1
              '@' . ($key == 'emails' ? 'Address' : 'Number') => $value,
            ];
          }
        }
        if($types){
          $record['Site']['Contact'][$type] = $types;
        }   
      }
      $record['Site']['Contact'] += [
        '@LastName' => $contact['lastname'],
        '@FirstName' => $contact['firstname'],
      ];
    } 
    return $this->encode($record);
  }
  
  protected function encode($record){
    $this->encoder->setRootNodeName('ContactRequest');
    $xml = $this->encoder->encode($record,'xml');
    // The SOAP request doesn't need the xml metadata 
    return str_replace('<?xml version="1.0"?>', '', $xml);
  }
  
  public function get($contact){ 
    $this->setMethod('GetContact');
    $record = [
      '@ContactType' => $contact['type'],
      '@ReferenceCode' => $contact['id'],
      '@DataProvider' => 'Drupal'
    ];
    $xml = $this->encode($record);
    return $this->execute($xml);
  }
  
  /**
   * Execute the SOAP request.
   * 
   * @return array
   */
  protected function execute($inputXml,$method='UpdateContact'){   
		$request['inputXml'] = ['any' => $inputXml]; 
    try {
      $response = call_user_func_array(array($this->client,$method),[$request]);  
    } catch (Exception $e){
      // @TODO
    }
    $xml = $response->{$method.'Result'}->any;
    $decoder = new XmlEncoder();
    $result = $decoder->decode($xml,'xml');
    $this->logger->notice($result['ContactResult']['@Value']);
  }

  /**
   * Create token for SOAP request.
   * 
   * The API key and the shared secret are used to create the unique signature
   * (or authentication token) which will be used to authenticate the caller
   *  of the Contact Web Service.
   *
   * @return string
   */
  protected function buildToken(){
    $key = $this->config->get('key');
    $secret = $this->config->get('secret');
    $upper = strtoupper($key); 
    $curDate = gmdate("Ymd",time()); 
    $curTime = gmdate("Hi",time());
    $string = "$upper|$secret|$curDate|$curTime"; 
    $md5string = md5($string);
    return "$md5string|$key";
  }
  
  /**
   * Set token in SOAP header
   */
  protected function setHeaders(){
    $token = $this->buildToken();
		$ns = 'BBConnect.Service.Contact';
		$headerbody = [
			'Token' => $token,
		];
		$header = new \SOAPHeader($ns, 'AuthToken', $headerbody);	
		$this->client->__setSoapHeaders($header);
  }

}
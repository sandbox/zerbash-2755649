<?php

namespace Drupal\blackboard_connect_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a form that configures Senior Systems module settings.
 */
class BlackboardConnectSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'blackboard_connect_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['blackboard_connect_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('blackboard_connect_api.settings');
    
    $form['info'] = [
      '#markup' => 'In order to use Blackboard Connect, you will need to obtain a key and a secret word from the Connect Integration Team.'
    ];
    
    $uri = 'https://connect.blackboardconnect.com/Admin/Sites';
    $url = Url::fromUri($uri);
    $link = Link::fromTextAndUrl($uri,$url)->toString();
    $form['local_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Local ID'),
      '#description' => $this->t('The local ID is your site name. See: ') . $link,
      '#default_value' => $config->get('local_id'),
    ];    
    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#default_value' => $config->get('key'),
    ];
    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Word'),
      '#default_value' => $config->get('secret'),
    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    foreach($values as $key => $value){
      $this->config('blackboard_connect_api.settings')
        ->set($key, $value)
        ->save();        
    }
    $message = $this->t('Your configuration has been saved.');
    drupal_set_message($message);
  }
 
}
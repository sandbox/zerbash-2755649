Blackboard Connect API
=======================

Blackboard Connect API allows Drupal to utilize Blackboard Connect's
Webservices. If you do not have the Contact Web Service API manual, you should
request it from the Data Integration team: connectintegration@blackboard.com

The parameters array will be converted to an appropriate xml document. Nested
arrays are treated slightly differently: If the array is associative, the key
will become a new element, with its values becoming attributes of that new 
element. If the key is numeric, multiple elements will be created using the
parent's associative key, and the attribute "Index" will be added, and given 
a value +1 the numeric key. In the example below, the array

  'Phone' => [
    [
      'Number' => '1234567890',  
    ],
    [
      'Number' => '1234567890',
    ],
  ),

will become

<Phone Index="1" Number="1234567890"/><Phone Index="2" Number="1234567890"/>


EXAMPLES

-------------------------
Retrieve a user
------------------------- 
  $params = [
    'DataProvider' => 'Client',
    'ReferenceCode' => 12345,
    'ContactType' => 'Faculty',
  ];

  $bca = \Drupal::service('blackboard_connect_api');
  $bca->setMethod('GetContact');
  $bca->setParams($params);
  $results = $bca->execute();


-------------------------    
Create or Update a user
-------------------------   

  $params = [
    'ReferenceCode' => 12345,
    'DataProvider' => 'Client',
    'Type' => 'Faculty',
    'LastName' => 'Smith',
    'FirstName' => 'John',
    'Demographics' => [
      'Gender' => 'M',
    ],
    'Phone' => [
      [
        'Number' => '1234567890',  
      ],
      [
        'Number' => '1234567890',
      ],
    ],
    'SMS' => [
      [
        'Number' => '1234567890',  
      ],
    ],
    'Email' => [
      [
        'Address' => 'johnsmith@example.com',    
      ],
      [
        'Address' => 'jsmith@example.com',
      ],
    ],
  ];

  $bca = \Drupal::service('blackboard_connect_api');
  $bca->setMethod('UpdateContact');
  $bca->setParams($params);
  $results = $bca->execute();

-------------------------    
Delete User
-------------------------  
  $params = [
    'ReferenceCode' => '12345',
    'DataProvider' => 'Client',
    'Type' => 'Faculty',
  ];
  $bca = \Drupal::service('blackboard_connect_api');
  $bca->setMethod('DeleteContact');
  $bca->setParams($params);
  $results = $bca->execute();

-------------------------    
Get Group Count
-------------------------  
  $params = [
    'Type' => 'Faculty',
  ];
  $bca = \Drupal::service('blackboard_connect_api');
  $bca->setMethod('GetContactCountByGroup');
  $bca->setParams($params);
  $results = $bca->execute();